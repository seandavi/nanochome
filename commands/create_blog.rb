usage       'create_blog [options]'
aliases     :blog
summary     'Create a blog entry in content/blog'
description 'Create a blog entry in content/blog'

option   :t, :title,  'blog title', :argument => :optional
option   :k, :keywords, 'keywords, comma-separated, no spaces', :argument => :optional
option   :s, :suffix, "Suffix to use for file, does not need to include the '.'", :argument => :optional

run do |opts, args, cmd|
  require 'uri'
  require 'date'
  d = DateTime.now()
  stuff2 = opts[:keywords].split(',')
  title = URI.escape(opts[:title].downcase.gsub(' ','_'))
  suffix = opts[:suffix] || 'rst'
  outfile = File.new("content/blog/#{title}.#{suffix}",'w')
  outfile.puts "---"
  outfile.puts "title: #{opts[:title]}"
  outfile.puts "kind: article"
  outfile.puts "author_name: Sean Davis"
  outfile.puts "author_uri: http://watson.nci.nih.gov/~sdavis/"
  outfile.puts "created_at: #{d.strftime("%A, %B %d, %Y")}"
  outfile.puts 'tags:'
  stuff2.each { |keyword|
    outfile.puts " - #{keyword}"
  }
  outfile.puts "---"
  outfile.puts ""
  outfile.puts "<% content_for :summary do %>"
  outfile.puts "summary here"
  outfile.puts "<% end %>"
  outfile.close()
  puts "Created file content/blog/#{title}.#{suffix} for editing."
end
