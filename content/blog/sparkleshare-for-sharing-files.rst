---
title: SparkleShare for Sharing Files--a DropBox Clone
created_at: Saturday, June 6, 2011
kind: article 
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/ 
tags:
 - utility
 - collaboration
 - file sharing
---

<% content_for :summary do %>
I have always found file sharing and collaboration tools quite interesing and have dabbled with wikis, Google Docs, Redmine, TRAC, and most recently Dropbox.  I really like DropBox as a concept, but wanted an open source solution that I could host locally--enter SparkleShare. 
<% end %>

I have always found file sharing and collaboration tools quite interesing and have dabbled with wikis, Google Docs, Redmine, TRAC, and most recently Dropbox.  I don't mean to compare apples and oranges here as the tools just mentioned have very different uses and features.  Dropbox is interesting for a couple of reasons, though.  It is simple file sharing (I watched a colleague set it up in 5 minutes yesterday) at a minimum.  Potentially more interesting things can be done with it because the files are available locally making building tools that take advantage of shared content a straightforward proposition.  The downside of Dropbox is the limitation in storage space and that the service is provided by a third party.

Enter `SparkleShare`_, which I learned about in `a recent posting on Biostar`_.  Simply set up a git repository on your server and that becomes the remote storage location.  Set up git on the client, install the SparkleShare client (currently linux and Mac only, but Windows is on the way) and away you go.  The connection from the client to the server location is via ssh so an open ssh port is necessary.  

.. _SparkleShare: http://sparkleshare.org
.. _a recent posting on Biostar: http://biostar.stackexchange.com/questions/822/how-do-you-manage-your-files-directories-for-your-projects

On the client (at least the Mac), the files show up in a folder, "~/SparkleShare".  A small client software needs to be running (just like DropBox) and the menu shows up in the top-right menu bar (again, just like DropBox).  Within the "~/SparkleShare" folder, for each remote folder there will be a folder with the same name.  Each of these is a working tree of a git repository, so the usual git commands and tools appear to work here as well.  

Since I am a big fan of git, open-source projects, and collaboration tools, I find SparkleShare an interesting and potentially useful tool.  SparkleShare is a bit rough-around-the-edges but it definitely has promise and is useable now.  

