---
title: A simple bootstrap-based knitr template
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/
created_at: Sunday, November 24, 2013
tags:
 - R
 - knitr
---

<% content_for :summary do %>
The R package, knitr, is a pretty nice system for publishing results of analyses and reports using the literate programming paradigm.  I found myself wanting to produce a pretty web-based output of my collaborator's data and analysis.  Having used twitter bootstrap for my own website and for other small html pages, I thought I would look into creating a small template for knitr based on bootstrap.
<% end %>

The R package, [knitr](http://yihui.name/knitr/), is a pretty nice system for publishing results of analyses and reports using the literate programming paradigm.  I found myself wanting to produce a pretty web-based output of my collaborator's data and analysis.  Having used [twitter bootstrap](http://getbootstrap.com/) for my own website (you're lookin' at it) and for other small html pages, I thought I would look into creating a small template for knitr based on bootstrap.

Here is what I came up with:

<script src="https://gist.github.com/7628066.js"></script>

To use this, simply grab the single file above and put it in a conventient place.  Then, run knitr to produce a bootstrap-based html file:

<pre lang="r">
library(knitr)
knit2html(system.file('examples/knitr-minimal.Rmd',package='knitr'),
          template='PATH/TO/bootstrap-template.html',
          title='knitr with bootstrap')
</pre>

You can see the output [here](/assets/blog/a_simple_bootstrap-based_knitr_template/knitr-minimal.html).


