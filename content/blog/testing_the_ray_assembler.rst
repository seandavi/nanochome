---
title: Testing the Ray assembler on Illumina paired-end data
created_at: Wednesday, April 4, 2011
kind: article 
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/ 
tags:
 - bioinformatics
 - genome assembly
 - sequencing
---

I have been wanting to play with a simple genome assembly software package for quite some time.  There are a number of them available, but I came across `this post`_ and thought I should give it a try, particularly since it is self-contained and purpose-built for parallel execution.

.. _this post: http://seqanswers.com/forums/showthread.php?t=4301

I grabbed the code from `the Ray home page`_ and installed on the `NIH biowulf cluster`_ using the following:

.. _the ray home page: http://sourceforge.net/apps/mediawiki/denovoassembler/index.php?title=Main_Page
.. _nih biowulf cluster: http://biowulf.nih.gov/

::

    export PATH=/usr/local/mpich2-intel64/bin/:$PATH
    export PATH=/usr/local/intel/Compiler/11.1/073/bin/intel64:$PATH
    tar -xvjf Ray-1.3.0.tar.bz2
    cd Ray-1.3.0
    more INSTALL
    ./configure --prefix=/data/sedavis/usr/local/Ray-1.3.0
    make
    make install

To run the software, I used a simple submit script:

::

    #!/bin/bash
    #PBS -N Ray
    # submit like:
    # qsub -v np=48 -l nodes=6:c8 submitRay.sh

    cd $PBS_O_WORKDIR

    export PATH=/usr/local/mpich2-intel64/bin:$PATH
    mpdboot -f $PBS_NODEFILE -n `cat $PBS_NODEFILE | wc -l`
    mpiexec -n $np /data/sedavis/usr/local/Ray-1.3.0/bin/Ray -k 31 -p 2_1_62B43AAXX.192_BUSTARD-2010-11-01.fastq.gz 2_2_62B43AAXX.192_BUSTARD-2010-11-01.fastq.gz -o 2_62B43AAXX.k31
    mpdallexit

The output was underwhelming with the largest contig being about 2kb despite some pretty high coverage.  I suspect that there is some adapter contamination in the reads, so the reads may need to be cleaned up a bit before use.  In any case, the output is a set of FASTA contigs.  I then used gmap to map those back to my genome of interest and was, again, a little disappointed with the resulting coverage.  I'll need to do a bit more playing with Ray, but I also need to try some more standard assemblers such as Velvet_, Abyss_, and Soapdenovo_.

.. _Velvet: http://molecularevolution.org/software/genomics/velvet
.. _Abyss: http://www.bcgsc.ca/platform/bioinfo/software/abyss
.. _Soapdenovo: http://soap.genomics.org.cn/soapdenovo.html


