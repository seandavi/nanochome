---
title: sqlalchemy with python 3 and mysql
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/
created_at: Thursday, March 21, 2013
tags:
 - mysql
 - sqlalchemy
 - python
 - python3
---

<% content_for :summary do %>
I have a few sqlalchemy-based projects that I have wanted to move over to
python 3.  Interestingly, the usual python-mysql is not supported.  There are 
a number of other options, but I settled on [Oracle's mysql-connector-python](https://pypi.python.org/pypi/mysql-connector-python/).  The usual pip incantation
gets you the library (pure python).
<% end %>

I have a few sqlalchemy-based projects that I have wanted to move over to
python 3.  Interestingly, the usual python-mysql is not supported.  There are 
a number of other options, but I settled on [Oracle's mysql-connector-python](https://pypi.python.org/pypi/mysql-connector-python/).  The usual pip incantation
gets you the library (pure python).

<pre>
pip install mysql-connector-python
</pre> 

Using the library is a matter of [using the right connection string](http://docs.sqlalchemy.org/en/latest/dialects/mysql.html#module-sqlalchemy.dialects.mysql.mysqlconnector):

<pre>
"mysql+mysqlconnector://..."
</pre>

