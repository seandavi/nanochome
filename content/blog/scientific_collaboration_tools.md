---
title: Scientific collaboration tools
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/
created_at: Friday, August 22, 2014
tags:
 - reproducible research
---

<% content_for :summary do %>
Reproducible research in a team environment implies that team members work collaboratively to produce knowledge.  Doing so in a productive and reproducible way has lead to a number of potentially useful tools and environments.  This post includes a sampling of such tools.  
<% end %>

Reproducible research in a team environment implies that team members work collaboratively to produce knowledge.  Doing so in a productive and reproducible way has lead to a number of potentially useful tools and environments.  This post includes a sampling of such tools. Please comment with additional suggestions about what you use and how you use it and I will try to add.

I have broken the list down into a few organizational categories realizing that there is plenty of overlap.

* toc
{:toc}

### Collaborative writing

- [Google docs](http://drive.google.com)
- [writeLaTeX](https://www.writelatex.com/) is a commercial solution for collaborative, online editing of LaTeX documents.
- [shareLaTeX](https://www.sharelatex.com/) is another commercial approach for collaborative, online editing of LaTeX documents.
- [StackEdit](https://stackedit.io/) is a MarkDown-based online editor that can open shared documents on google drive or dropbox.
- [Authorea](https://www.authorea.com/) is self-described as "the collaborative platform for research. Write and manage your technical documents in one place." It supports markdown and LaTeX.
- [EtherPad](http://etherpad.org) is an online editor, similar to google docs, but not requiring *any* login. Export to multiple file formats, though not markdown as of this writing. There are multiple public instances, but the software is open source and runs on node.js for local installation.

### Brainstorming

- [whiteboardfox](http://whiteboardfox.com/) allows users to share a virtual whiteboard in real-time using any modern web browser. *No login required*.
- [skype](http://www.skype.com) is an old favorite.
- [Google Hangouts](https://plus.google.com/hangouts) is Google's integrated collaboration site including chatting, video calling, file sharing, etc. It works relatively seamlessly on multiple devices including android and iOS.

### Data and file sharing

- [globus/gridFTP](https://www.globus.org/) is a suite of tools and technologies that enables high-performance, robust file transfers. The software supporting this capability is installed locally (even on a laptop). For a small fee (<$10 per month), a user can share files or directories with other globus users. Unlike many other file sharing approaches mentioned here, this one will work for *very large* file sharing.
- [Dropbox](http://www.dropbox.com/) -- you have probably heard of this one.
- [Google Docs/Google Drive](http://drive.google.com/) -- no explanation required.
- [ownCloud](http://owncloud.org/) provides access to your data through a web interface or WebDAV while providing a platform to easily view, sync and share across devices—all under your control. ownCloud’s open architecture is extensible via a simple but powerful API for applications and plugins and works with any storage. This software runs on local hardware (requires installation and configuration).
- [figshare](http://figshare.com)
- [Sage Synapse](http://www.synapse.org/)
- [transfer.sh](http://transfer.sh/) allows you upload and download files without complexity from your shell or browser. Just upload the file by dropping it to the transfer.sh page, curl or any other command using PUT to their server. They will return a unique (obscure) shareable url, which will expire within 2 weeks.


### Collaborative coding

- [git](http://git-scm.com/) is the mother of all collaborative coding tools. It forms the basis for multiple online sites for social coding.
- [github](http://github.com/) is the most popular code sharing and collaborative coding site on the planet. Social coding features such as pull requests, issues, comments, "star", and "watch" allow even non-coders to follow and contribute to code projects. 
- [bitbucket](http://bitbucket.org) is very similar to github, though not as popular. It has free private repositories, though, so it is great for early or private development efforts that can later be exposed to the public.
- [coderscrowd](http://coderscrowd.com/) describes itself as "a platform to discuss your problems and share your skills". It is kind of like a stackoverflow, but with live code.

### General communication and project management

- [slack](http://slack.com) (via [@thatdnaguy](https://twitter.com/thatdnaguy)) is a platform for team communication: everything in one place, instantly searchable, available wherever you go. It has integration capabilities with dozens of services including twitter, github, etc. And it is free! 


### Online lists of collaboration software

There are many other catalogs of collaboration software available online. Some are more focused on project management, etc., but I'll drop those below.

- [The 20 best tools for online collaboration](http://www.creativebloq.com/design/online-collaboration-tools-912855)
