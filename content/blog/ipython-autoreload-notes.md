---
title: autoreload in ipython to ease development
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/
created_at: Thursday, May 9, 2013
tags:
 - ipython
 - autoreload
 - python
---

<% content_for :summary do %>
New versions of ipython (after 0.12) ship with a convenient autoreload functionality.  This is just a note-to-self about usage.
<% end %>

New versions of ipython (after 0.12) ship with a convenient autoreload functionality.  This is just a note-to-self about usage.

From the [ipython documentation](http://ipython.org/ipython-doc/dev/config/extensions/autoreload.html "Ipython autoreload documentation"):


    In [1]: %load_ext autoreload

    In [2]: %autoreload 2

    In [3]: from foo import some_function

    In [4]: some_function()
    Out[4]: 42

    In [5]: # open foo.py in an editor and change some_function to return 43

    In [6]: some_function()
    Out[6]: 43

And here are the usage notes:

`%autoreload`
: Reload all modules (except those excluded by %aimport) automatically now.

`%autoreload 0`
: Disable automatic reloading.

`%autoreload 1`
: Reload all modules imported with %aimport every time before executing the Python code typed.

`%autoreload 2`
: Reload all modules (except those excluded by %aimport) every time before executing the Python code typed.

`%aimport`
: List modules which are to be automatically imported or not to be imported.

`%aimport foo`
: Import module ‘foo’ and mark it to be autoreloaded for %autoreload 1

`%aimport -foo`
: Mark module ‘foo’ to not be autoreloaded.