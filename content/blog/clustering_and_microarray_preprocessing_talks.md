---
title: Clustering and Microarray Preprocessing Talks
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/
created_at: Wednesday, November 27, 2013
tags:
 - bioinformatics
 - teaching
---

<% content_for :summary do %>
The good folks at Cold Spring Harbor have made the lectures from the CSHL Statistical Analysis of Genomic Data available for public viewing.  A couple of my talks give a high-level overview of microarray preprocessing and clustering.
<% end %>

The good folks at [Cold Spring Harbor](https://www.youtube.com/user/LeadingStrand) have made the lectures from the CSHL Statistical Analysis of Genomic Data available for public viewing.  A couple of my talks give a high-level overview of microarray preprocessing and clustering.

## Microarray Preprocessing
<iframe width="853" height="480" src="//www.youtube.com/embed/8iYeXpJUjhY" frameborder="0" allowfullscreen></iframe>

## Clustering Overview

<iframe width="853" height="480" src="//www.youtube.com/embed/tln64P-w_8c" frameborder="0" allowfullscreen></iframe>
