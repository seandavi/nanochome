---
title: Globus Connect Multiuser Setup Notes
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/
created_at: Sunday, August 24, 2014
tags:
 - IT
 - data management
---

<% content_for :summary do %>
Globus Connect enables your system to use the Globus file transfer and sharing service. It makes it simple to create a Globus endpoint on practically any system, from a personal laptop to a national supercomputer. Globus Connect is free to install and use for users at non-profit research and education institutions. This presents very brief notes for setting up globus connect multiuser.
<% end %>

Globus Connect enables your system to use the Globus file transfer and sharing service. It makes it simple to create a Globus endpoint on practically any system, from a personal laptop to a national supercomputer. Globus Connect is free to install and use for users at non-profit research and education institutions.

Here are very simple/sketchy notes on setup of globus-connect-multiuser, a server appropriate for hosting an endpoint on a multiuser machine such as a NAS or other file store. These notes are derived from [this support forum entry](https://support.globus.org/entries/23857088-Installing-Globus-Connect-Server).

~~~ bash
rpm -hUv http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
yum install globus-connect-multiuser
~~~

EDIT /etc/globus-connect-multiuser.conf

- Change username 
- Change password 
- Change public to True

Then do:

~~~ bash
globus-connect-multiuser-setup
~~~

Any changes to the config file will require another:

~~~ bash
globus-connect-multiuser-setup
~~~

(which will simply restart servers, etc.)

Make sure that appropriate ports are open in any firewall. 
