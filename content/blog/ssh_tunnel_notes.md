---
title: ssh tunnel notes
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/
created_at: Wednesday, August 07, 2013
tags:
 - ssh
---

<% content_for :summary do %>
Tunneling ssh connections is a convenient way to encrypt network traffic, use transparent compression on slow networks, and work through firewalls.  While this information is available online in multiple places, I put it here for convenient reference.
<% end %>

Tunneling ssh connections is a convenient way to encrypt network traffic, use transparent compression on slow networks, and work through firewalls.  While this information is available online in multiple places, I put it here for convenient reference.

I have a number of servers that sit behind a firewall and a single border server with an open ssh port (22).  Working from home on my laptop, I want to connect to those servers inside the firewall by tunneling through the border machine's ssh connection.  I use a script to set up the ssh tunnel to multiple machines:

~~~~~
#!/bin/bash
ssh -C \
    -L 2022:machine1.example.com:22 \
    -L 2023:machine2.example.com:22 \
    -L 2024:machine3.example.com:22 \
    -L 2025:machine4.example.com:22 \
    -L 5434:machine2.example.com:5432 \
       username@bordermachine.example.com
~~~~~

The `-C` flag turns on compression on the tunnels.  The tunnels themselves (and there can be multiple) are set up using the -L syntax.  In this case, the first four tunnels will connect to port 22 on machine1-machine4, respectively.  The last connects to port 5432 on machine2.  The `username@bordermachine.example.com` establishes the connection to the "border" machine via the normal port 22 (default).

The magic happens if we do:

~~~~~
ssh -p 2022 localhost username
~~~~~

This will connect to port 22 on machine1.example.com using the username "username".  

In order to reduce this typing, one can set up a config file in `~/.ssh/config`.  An example entry might look like:

~~~~~
host rmachine1
Hostname machine1.example.com.
User username
Port 2022
~~~~~

The `host` does not need to be a resolvable host name.  Now, after establishing the tunnels using the ssh tunnel command, connecting to machine1.example.com via the tunnel and with compression is as simple as doing:

~~~~~
ssh rmachine1
~~~~~


