---
title: Protected nginx directory with ldap and uwsgi
created_at: Tuesday, December 6, 2011
kind: article 
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/ 
tags:
 - nginx
 - ldap
 - uwsgi
---

<% content_for :summary do %>
After years of Apache, I have switched over to nginx for proxying and for serving as much content as possible.  Recently, I had a need to have basic auth against ldap.  On Apache, that meant adding mod_auth_ldap or another module.  On nginx, that meant proxying through to a process to do the authentication and authorization and then to serve up an X-Accel-Redirect.  This post is just a few notes on that process.
<% end %>

After years of Apache, I have switched over to nginx for proxying and for serving as much content as possible.  Recently, I had a need to have basic auth against ldap.  On Apache, that meant adding mod_auth_ldap or another module.  On nginx, that meant proxying through to a process to do the authentication and authorization and then to serve up an X-Accel-Redirect.  This post is just a few notes on that process.  

I had several large files (the largest about 40GB) that I wanted to serve as static files over https and using basic authentication against our local ldap server.  Our nginx server is running on a Mac, so some of the normal tutorials that assume a linux package manager simply do not apply.  Basically, the workflow looked like:

- put the files to be served in a directory to be protected
- install nginx
- install uwsgi
- install a few python packages
- write a small authentication app in python
- run the authentication app using uwsgi

I am a huge supporter of the `homebrew package system for the Mac <https://github.com/mxcl/homebrew>`_.  Installing nginx was pretty much as simple as:

.. code-block:: r

    brew install nginx

The uWSGI server is describes `on the website <http://projects.unbit.it/uwsgi/>`_ as "a fast, self-healing and developer/sysadmin-friendly application container server coded in pure C."  It is quite flexible and, importantly, can use a streamlined protocol for talking to nginx (not having to talk http with all the associated header parsing, etc.).  Figuring out how to install uWSGI on a Mac was a bit challenging.  The uWSGI source code includes a compile flag, -Werror, that converts warnings during compilation to errors.  After downloading `the source <http://projects.unbit.it/downloads/uwsgi-0.9.9.3.tar.gz>`_, I grepped for 'Werror' in the top-level directory--it was in uwsgiconfig.py--and then deleted that flag.  After that:

.. code-block:: 

    python setup.py install

did the trick. 

Installing `python-ldap <http://www.python-ldap.org/>`_ was also a bit challenging, but `this link <http://stackoverflow.com/a/6925715/459633>`_ got me what I needed:

.. code-block:: 

    pip install python-ldap==2.3.13

Next, I needed a small script to do the authentication.  I chose to use the `Flask microframework <http://flask.pocoo.org/>`_ to get this done easily.  The code (with a few obscured details) is here:

.. code-block:: python


    from flask import Flask
    import ldap

    ldapserver = 'exampleldap.com'
    base = 'OU=NIH,OU=AD,DC=nih,DC=gov'
    l=ldap.open(ldapserver)

    from functools import wraps
    from flask import request, Response


    def check_auth(username, password):
	"""This function is called to check if a username /
	password combination is valid.
	"""
	retval = None
	try:
	    l.simple_bind_s(username+"@nih.gov",password)
	    return True
	except ldap.INVALID_CREDENTIALS:
	    return False

    def authenticate():
	"""Sends a 401 response that enables basic auth"""
	return Response(
	'Could not verify your access level for that URL.\n'
	'You have to login with proper credentials', 401,
	{'WWW-Authenticate': 'Basic realm="Login Required"'})

    def requires_auth(f):
	@wraps(f)
	def decorated(*args, **kwargs):
	    auth = request.authorization
	    if not auth or not check_auth(auth.username, auth.password):
		return authenticate()
	    return f(*args, **kwargs)
	return decorated


    app = Flask(__name__)

    @app.route('/data/osteosarcoma/target/bam/<fname>')
    @requires_auth
    def hello_world(fname):
        response = Response()
    	response.headers['X-Accel-Redirect']='/d/osteosarcoma/target/bam/%s' % fname
	response.headers['Content-Type']='application/octet-stream'
	return response

    if __name__ == '__main__':
	app.run()


I'll leave it to the reader to look into the details of the code.  To set up nginx, it was as simple as making a couple of changes to the config file:

.. code-block:: 

        location /d/ {
            internal;
            root /path/to/www;
        }

        location /data/ {
            include uwsgi_params;
            uwsgi_pass 127.0.0.1:3031;
        }

With this block inside a server block, the files in /path/to/www/d/osteosarcoma/target/bam/ will be served when a file is requested at url /data/osteosarcoma/target/bam/..., but only after the user has been authenticated.  But, we still need to hook up the uwsgi server processes, which will do the auth and send the x-accel-redirect.  My files are binary, so I have to set the Content-Type to 'application/octet-stream' since flask defaults to 'text/plain'.  

I have not yet figured out the best way to run the uwsgi process on a Mac as a daemon, but this command gets things going and is working for now.  

.. code-block:: bash

    uwsgi --socket 127.0.0.1:3031 --file app.py --callable app --processes 2 >& /tmp/uwsgi.log

I hope this sketchy post is helpful for someone besides me, but it does serve to remind me of some details of the process.  Something will break down the road and I have to start someplace to fix it.

