---
title: A simple shiny app for exploring SRAdb
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/
created_at: Friday, July 19, 2013
tags:
 - R
 - web
---

<% content_for :summary do %>
I show an example of using the shiny R package to examine the SRAdb package SQLite database schema.
<% end %>

At the Bioc2013 conference this year, there was quite a bit of interest and hype associated with the [shiny R package](http://www.rstudio.com/shiny/). 
As a simple experiment, I wrote some quick (took just a few minutes) to examine the  [SRAdb package](http://www.bioconductor.org/packages/release/bioc/html/SRAdb.html) SQLite database schema using a shiny web app.

Starting at the end, here is a screen shot of the resulting application.

<img src="/assets/images/sradb.shiny.png"/>

To reproduce the application, you'll need to 

1. Install R and Bioconductor, and the shiny and SRAdb packages in particular.  
2. Place the two files below into a folder called "shinyApp".  
3. Use the `getSRAdbFile()` function to download the sqlite file to the directory above "shinyApp" directory.
4. Load the shiny library.
5. Execute `runApp('shinyApp')`.

<script src="https://gist.github.com/seandavi/6043274.js"></script>

