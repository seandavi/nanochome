---
title: Installing R Source Packages on Mac OS with XCode 5.x
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/
created_at: Thursday, November 07, 2013
tags:
 - rstats
 - mac
---

<% content_for :summary do %>
Many of us using R on the Mac have been spoiled by having available a stable build environment, even for source packages. I recently upgraded to OS X 10.8 and then upgraded to XCode 5.x.  Out-of-the-box, I ran into compile errors related to a missing compiler; a solution is given.
<% end %>

Many of us using R on the Mac have been spoiled by having available a stable build environment, even for source packages.  I recently upgraded to OS X 10.8 and then upgraded to XCode 5.x.  Trying to install source packages yielded a bunch of:

<pre>
* installing *source* package 'colorspace' ...
** package 'colorspace' successfully unpacked and MD5 sums checked
** libs
llvm-gcc-4.2 -arch x86_64 -std=gnu99 -I/Library/Frameworks/R.framework/Resources/include -DNDEBUG  -I/usr/local/include    -fPIC  -mtune=core2 -g -O2  -c colorspace.c -o colorspace.o
make: llvm-gcc-4.2: No such file or directory
make: *** [colorspace.o] Error 1
ERROR: compilation failed for package 'colorspace'
</pre>

Note that `llvm-gcc-4.2` is not available on my Mac.  Just a little searching got me to [this post on stackoverflow](http://stackoverflow.com/questions/19503995/error-when-with-xcode-5-0-and-rcpp-command-line-tools-are-installed/19505252#19505252).  I created the file `~/.R/Makevars` with this content:

<pre>
CC=clang
CXX=clang++
</pre>

R installation from source worked after that.  I have no idea why building from source is currently broken out-of-the-box, but for those of use developing packages, it is problematic; I am happy to be back to my usual and expected stable build environment.

* R version: 3.1.0 (R-devel) downloaded from r.research.att.com
* XCode version: 5.0.0
* OSX version: 10.8.5