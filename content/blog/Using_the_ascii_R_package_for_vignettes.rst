---
title: Using the ascii R package for building vignettes
created_at: Monday, September 26, 2011
kind: article 
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/ 
tags:
 - R
 - Reproducible Research
 - org-mode
 - restructured text
 - textile
 - Sweave
 - asciidoc
 - pandoc
---

<% content_for :summary do %>
Building vignettes for R is a great way of generating reproducible research.  The Sweave R function does just this.  Out-of-the-box, Sweave expects documents to be written in Latex.  The ascii package extends the possibilities to include asciidoc, textile, Org, restructured text, and many others via pandoc. 
<% end %>

Building vignettes for R is a great way of generating reproducible research.  The Sweave R function does just this.  Out-of-the-box, Sweave expects documents to be written in Latex.  The `ascii package <http://cran.r-project.org/web/packages/ascii/index.html>`_ extends the possibilities to include asciidoc, textile, Org, restructured text, and many others via pandoc. 

