---
title: Publicly Available Human Genome Variant Databases
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/
created_at: Tuesday, October 21, 2014
tags:
 - genomics
 - variants
---

<% content_for :summary do %>
Back in the day, there was one variant database, and only one--dbSNP.  Then came HapMap and 1000Genomes. Now, there are many such resources, each of which can be useful for annotating and filtering variants found in our own data. Here, I have just pulled together a few such resources and a little relevant information about each.
<% end %>

Back in the day, there was one variant database, and only one--[dbSNP](http://www.ncbi.nlm.nih.gov/SNP/).  Then came HapMap and then [1000Genomes](http://www.1000genomes.org/). Now, there are many such resources, each of which can be useful for annotating and filtering variants found in our own data. Here, I just pulled together a few such resources and a little relevant information about each.

The download links were valid as of this writing and I will try to update them over time, as many of these resources are still growing. When possible, I have pointed the download links to VCF file locations.

If there are other resources of interest to the community, please drop me a comment and I will try to add them as they come in.

### dbSNP

- website: [http://www.ncbi.nlm.nih.gov/SNP/](http://www.ncbi.nlm.nih.gov/SNP/)
- downloads: [ftp](ftp://ftp.ncbi.nlm.nih.gov/snp/organisms/human_9606/VCF/)

### Exome Aggregation Consortium

- website: [http://exac.broadinstitute.org/](http://exac.broadinstitute.org/)
- downloads: [ftp](ftp://ftp.broadinstitute.org/pub/ExAC_release/release0.1/)
- samples: 65,000

### 1000 Genomes

- website: [http://www.1000genomes.org](http://www.1000genomes.org)
- downloads:
  - NCBI: [ftp](ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/) or [Aspera](http://www.ncbi.nlm.nih.gov/projects/faspftp/1000genomes/)
  - EBI: [ftp](ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/) or [Aspera](http://www.1000genomes.org/aspera)
  - Globus endpoint: "ebi#1000genomes" described [here](http://www.1000genomes.org/category/frequently-asked-questions/globus)
- samples: About 1100

### ClinVar

- website: [http://www.ncbi.nlm.nih.gov/clinvar/](http://www.ncbi.nlm.nih.gov/clinvar/)
- downloads: [ftp](ftp://ftp.ncbi.nlm.nih.gov/snp/organisms/human_9606/VCF/)

### Exome Sequencing Project

- website: [http://evs.gs.washington.edu/EVS/](http://evs.gs.washington.edu/EVS/)
- downloads: [http](http://evs.gs.washington.edu/EVS/)
- samples: about 6500

### Genome of the Netherlands

- website: [http://www.nlgenome.nl/](http://www.nlgenome.nl/)
- downloads: [http](https://molgenis26.target.rug.nl/downloads/gonl_public/variants/release5/)
- samples: About 800

### UK10K

- website: [http://www.uk10k.org/](http://www.uk10k.org/)
- downloads: [ftp](ftp://ngs.sanger.ac.uk/production/uk10k/UK10K_COHORT/REL-2012-06-02/)
- samples: About 2000

### GEUVADIS: Genetic European Variation in Health and Disease

- website: [http://www.geuvadis.org/web/geuvadis/home](http://www.geuvadis.org/web/geuvadis/home)
- downloads: [ftp](ftp://ftp.ebi.ac.uk/pub/databases/eva/PRJEB6042/ERZX00026/)
- samples: about 900

### COSMIC: Catalogue of Somatic Mutations in Cancer:

- website: [http://cancer.sanger.ac.uk/cancergenome/projects/cosmic/](http://cancer.sanger.ac.uk/cancergenome/projects/cosmic/)
- downloads: [http](http://cancer.sanger.ac.uk/files/cosmic/current_release/VCF/)
