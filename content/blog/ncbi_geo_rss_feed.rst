---
title: NCBI Gene Expression Omnibus (GEO) RSS feed for new data
created_at: Monday, April 9, 2011
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/ 
tags:
 - bioinformatics
 - GEO
---

<% content_for :summary do %>
The NCBI Gene Expression Omnibus now provides an RSS feed for new GEO
Series records.  Subscribing to the feed provides a constant stream of
new submissions to GEO. 
<% end %>

The NCBI Gene Expression Omnibus now provides an RSS feed for new GEO
Series records.  Subscribing to the feed provides a constant stream of
new submissions to GEO.  I used to maintain this RSS feed locally but
have replaced my feed with theirs.  To subscribe, point your reader
software at either `my local feed`_ or at the `official feed
location`_.

.. _my local feed: http://feeds.feedburner.com/ncbiGeo
.. _official feed location: http://www.ncbi.nlm.nih.gov/geo/feed/series/

