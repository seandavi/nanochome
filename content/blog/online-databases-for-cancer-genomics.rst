---
title: Online Databases for Cancer Genomics
created_at: Thursday, September 22, 2011
kind: article 
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/ 
tags:
 - cancer genomics
 - bioinformatics
 - online databases
---

<% content_for :summary do %>
The venues for cancer research have always included both the laboratory and the clinic.  The internet is now a third venue that is becoming increasingly important.  Patients use the internet to find support, information, and even care.  Clinicians use it for delivering care and staying up on research (and even to communicate with patients).  Cancer researchers also use online resources.  Here, I give a short list of online resources for cancer researchers, some of which are a bit off the beaten path.  This is not a comprehensive list, but a couple of the resources I mention are (comprehensive lists, that is).
<% end %>

The venues for cancer research have always included both the laboratory and the clinic.  The internet is now a third venue that is becoming increasingly important.  Patients use the internet to find support, information, and even care.  Clinicians use it for delivering care and staying up on research (and even to communicate with patients).  Cancer researchers also use online resources.  Here, I give a short list of online resources for cancer researchers, some of which are a bit off the beaten path.  This is not a comprehensive list, but a couple of the resources I mention are (comprehensive lists, that is).

Catalog of Somatic Mutations in Cancer (COSMIC)
-----------------------------------------------------
- URL: http://www.sanger.ac.uk/genetics/CGP/cosmic/

The COSMIC database is a curated database of somatic variants in human cancer.  It contains SNVs, some gene fusion information, and information regarding copy number.  

From the COSMIC website:

    All cancers arise as a result of the acquisition of a series of fixed DNA sequence abnormalities, mutations, many of which ultimately confer a growth advantage upon the cells in which they have occurred. There is a vast amount of information available in the published scientific literature about these changes. COSMIC is designed to store and display somatic mutation information and related details and contains information relating to human cancers.  Contains information on publications, samples and mutations. Includes samples which have been found to be negative for mutations during screening therefore enabling frequency data to be calculated for mutations in different genes in different cancer types.  Samples entered include benign neoplasms and other benign proliferations, in situ and invasive tumours, recurrences, metastases and cancer cell lines.  The mutation data and associated information is extracted from the primary literature and entered into the COSMIC database. In order to provide a consistent view of the data a histology and tissue ontology has been created and all mutations are mapped to a single version of each gene. The data can be queried by tissue, histology or gene and displayed as a graph, as a table or exported in various formats.

Some features of interest include:

- `Cancer Gene Census <http://www.sanger.ac.uk/genetics/CGP/Census/>`_
- `Browse by Gene <http://www.sanger.ac.uk/perl/genetics/CGP/cosmic?action=bygene>`_
- `Browse by Tissue <http://www.sanger.ac.uk/perl/genetics/CGP/cosmic?action=bytissue>`_
- `Dataset download <ftp://ftp.sanger.ac.uk/pub/CGP/cosmic>`_
- `Biomart interface <http://www.sanger.ac.uk/genetics/CGP/cosmic/biomart/martview/>`_, but more on this later

CaSNP - Copy Number in Cancer
---------------------------------------------
URL: http://cistrome.dfci.harvard.edu/CaSNP/index/

From the CaSNP website:

    CaSNP is a comprehensive collection of copy number alteration (CNA) from SNP arrays.  It collects 11,485 Affymetrix SNP arrays of 34 different cancer types in 105 studies to profile the genome-wide CNA and SNP in each.    This includes all the cancer SNP profiles using Affymetrix SNP arrays (10K to 6.0) with raw data from GEO, with additional arrays from the TCGA consortium and a few individual publications.  All CNA data stored in CaSNP is generated from raw data analyzed by dCHIP-SNP software.

Chimerdb
----------------
URL: http://ercsb.ewha.ac.kr:8080/FusionGene/

From the Chimerdb website:

    Chromosome translocation and gene fusion are frequent events in the human genome and are often the cause of many types of tumor. ChimerDB is designed to be a knowledgebase of fusion transcripts collected from various public resources such as the Sanger CGP, OMIM, PubMed, and Mitelman database. 

Features include:

- `Search by gene/gene fusion <http://ercsb.ewha.ac.kr:8080/FusionGene/Search.jsp>`_
- `Bulk data download <http://ercsb.ewha.ac.kr:8080/FusionGene/Download.jsp>`_

Biomart
----------
URL:  http://www.biomart.org/

From the biomart website:

    BioMart is a query-oriented data management system developed jointly by the Ontario Institute for Cancer Research (OICR) and the European Bioinformatics Institute (EBI). The system can be used with any type of data and is particularly suited for providing 'data mining' like searches of complex descriptive data. BioMart comes with an 'out of the box' website that can be installed, configured and customised according to user requirements. Further access is provided by graphical and text based applications or programmatically using web services or API written in Perl and Java. BioMart has built-in support for query optimisation and data federation and in addition can be configured to work as a DAS 1.5 Annotation server. The process of converting a data source into BioMart format is fully automated by the tools included in the package. Currently supported RDBMS platforms are MySQL, Oracle and Postgres. 

- Ensembl Genes
   URL:  http://useast.ensembl.org/biomart/martview/

- REACTOME
   URL:  http://www.reactome.org:5555/biomart/martview/

- COSMIC
   URL:  http://www.sanger.ac.uk/genetics/CGP/cosmic/biomart/martview/


UCSC Cancer Genome Browser
------------------------------------------
URL:  https://genome-cancer.ucsc.edu/

From the UCSC Cancer Genomics Browser Website:

    Welcome to the UCSC Cancer Genomics Browser. The browser is a suite of web-based tools to visualize, integrate and analyze cancer genomics and its associated clinical data. It is developed and maintained by the UCSC Cancer Genomics Group, led by David Haussler and Jim Kent, working closely with the UCSC Human Genome Browser team at the Center for Biomolecular Science and Engineering (CBSE) at the University of California Santa Cruz (UCSC). 


DAVID/EASE
-------------------------
URL:  http://david.abcc.ncifcrf.gov/

From `This paper <http://www.nature.com/nprot/journal/v4/n1/abs/nprot.2008.211.html>`_

    DAVID bioinformatics resources consists of an integrated biological knowledgebase and analytic tools aimed at systematically extracting biological meaning from large gene/protein lists. This protocol explains how to use DAVID, a high-throughput and integrated data-mining environment, to analyze gene lists derived from high-throughput genomic experiments. The procedure first requires uploading a gene list containing any number of common gene identifiers followed by analysis using one or more text and pathway-mining tools such as gene functional classification, functional annotation chart or clustering and functional annotation table. By following this protocol, investigators are able to gain an in-depth understanding of the biological themes in lists of genes that are enriched in genome-scale studies.

Galaxy
----------
URL:  http://main.g2.bx.psu.edu/

From the Galaxy website:

    Galaxy is an open, web-based platform for data intensive biomedical research. Whether on this free public server or your own instance, you can perform, reproduce, and share complete analyses. The Galaxy team is a part of BX at Penn State, and the Biology and Mathematics and Computer Science departments at Emory University. The Galaxy Project is supported in part by NSF, NHGRI, The Huck Institutes of the Life Sciences, The Institute for CyberScience at Penn State, and Emory University.


Database of databases
---------------------------
`Nucleic Acids Research database issue <http://www.oxfordjournals.org/nar/database/c/>`_

`Database Journal <http://database.oxfordjournals.org/>`_


