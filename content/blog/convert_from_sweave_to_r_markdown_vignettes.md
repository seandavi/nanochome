---
title: Convert from Sweave to R markdown vignettes
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/
created_at: Sunday, September 21, 2014
tags:
 - R
---

<% content_for :summary do %>
I recently converted my GEOquery vignette from Sweave to R markdown and created a few notes on the process.
<% end %>

I recently converted my [GEOquery vignette](https://github.com/seandavi/GEOquery/blob/master/vignettes/GEOquery.Rmd) from Sweave to R markdown and created a few notes on the process. Why, you ask? Because it is now easy to do this (and because I like to try new things). Until relatively recently, using something other than Sweave for vignettes was technically pretty challenging, requiring nasty hacks, Makefiles, and the like. As things stand, functionality to build HTML vignettes using knitr and rmarkdown is included out-of-the-box with R.

The [DESCRIPTION file](https://github.com/seandavi/GEOquery/blob/master/DESCRIPTION) needs a couple of minor adjustments:

    Suggests: knitr, rmarkdown, ...
    VignetteBuilder: knitr

Vignette files now go in the directory `PACKAGEROOT/vignettes`. A standard `.Rmd` file in that directory will become our vignette. In addition, a couple of legacy tags (from Sweave days) are required to tell R what to do with the vignette. The yaml header and (html commented) tags [look like this](https://github.com/seandavi/GEOquery/blob/master/vignettes/GEOquery.Rmd) for my GEOquery package:

    ---
    title: "Using the GEOquery Package"
    author: "Sean Davis"
    date: September 21, 2014
    output:
      html_document:
        toc: true
        number_sections: true
        theme: united
        highlight: pygments
    ---
    <!--
    %\VignetteEngine{knitr::rmarkdown}
    %\VignetteIndexEntry{Using GEOquery}
    -->

A standard `R CMD build` on the package will now build an [HTML version of the vignette and index it appropriately](http://www.bioconductor.org/packages/devel/bioc/html/GEOquery.html).
