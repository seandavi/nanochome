---
title: Software cataloging made simple--the R DESCRIPTION file
kind: article
author_name: Sean Davis
author_uri: http://watson.nci.nih.gov/~sdavis/
created_at: Saturday, August 30, 2014
tags:
 - R
---

<% content_for :summary do %>
The NIH has just released a Request for Information: Input on Information Resources for Data-Related Standards Widely Used in Biomedical Science. While the R package system is no panacea, it *is* a successful system by many measures and represents a practical approach to metadata management associated with software. At the core of this system is the R DESCRIPTION file that is computable, human consumable, and extensible. 
<% end %>

The NIH has just released a Request for Information: [Input on Information Resources for Data-Related Standards Widely Used in Biomedical Science](http://grants.nih.gov/grants/guide/notice-files/NOT-CA-14-054.html). One goal (I hope) is to make data and software more discoverable and searchable. While the R package system is no panacea, it *is* a successful system by many measures and represents a practical approach to metadata management associated with software. At the core of this system is the R DESCRIPTION file that is computable, human consumable, and extensible. Most importantly, it is **simple**. 

[Bioconductor](http://bioconductor.org) is perhaps the largest open-source software project dedicated to biological data analysis with an active user base numbering several thousand (based on mailing list subscription and download statistics).  Bioconductor is built on the [R statistical software platform](http://www.r-project.org/) that enjoys an even larger user base.  The success of R and Bioconductor can be traced back to the simple extensibility of R and, critically, the availability of well-maintained repositories of these extension packages.  The number of such packages likely now exceeds several thousand, but a simple, required DESCRIPTION file, shipped with every package, enables both computable and human consumable metadata maintenance and searching.  The DESCRIPTION file is a simple tag-value format that includes minimal information about each software project but is easily extensible and can include bug reporting mechanisms, support, tagging, free text description, licensing, versioning, and even dependencies.  An example [DESCRIPTION file](http://cran.r-project.org/doc/manuals/r-release/R-exts.html#The-DESCRIPTION-file) looks like:


	Package: GEOquery
	Type: Package
	Title: Get data from NCBI Gene Expression Omnibus (GEO)
	Version: 2.31.1
	Date: 2014-07-10
	Author: Sean Davis <sdavis2@mail.nih.gov>
	Maintainer: Sean Davis <sdavis2@mail.nih.gov>
	BugReports: https://github.com/seandavi/GEOquery/issues/new
	Depends: methods, Biobase
	Imports: XML, RCurl
	Suggests: limma
	URL: https://github.com/seandavi/GEOquery
	biocViews: Microarray, DataImport, OneChannel, TwoChannel, SAGE
	Description: The NCBI Gene Expression Omnibus (GEO) is a public repository of microarray data.  Given the rich and varied nature of this resource, it is only natural to want to apply BioConductor tools to these data.  GEOquery is the bridge between GEO and BioConductor.
	License: GPL-2


There is a real temptation to think of large problems like software cataloging and discoverability as requiring complex solutions. In some cases, simple suffices. Such software metadata files exist for other packaging systems and languages such as ruby-gems (ruby), pypi (python), and CPAN (perl).  In developing standards with respect to software cataloging, it will be important to be aware of existing approaches and to work with developer communities to enhance these existing solutions, and, where possible, **keep it simple**.  

