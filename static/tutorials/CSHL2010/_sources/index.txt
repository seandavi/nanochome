.. CSHL 2010 documentation master file, created by
   sphinx-quickstart on Mon Jun 28 22:55:02 2010.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CSHL 2010's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2

   publicRepos
   methylation
   TCGA

`PDF version of course materials <CSHL2010.pdf>`_

