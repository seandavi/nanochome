\select@language {english}
\contentsline {section}{1~~~Background}{1}{section*.4}
\contentsline {section}{2~~~The Data}{2}{section*.6}
\contentsline {subsection}{2.1~~~Methylation data preparation}{6}{section*.8}
\contentsline {subsection}{2.2~~~CGH data preparation}{7}{section*.10}
\contentsline {subsection}{2.3~~~Expression data preparation}{9}{section*.12}
\contentsline {section}{3~~~Analyses}{10}{section*.14}
\contentsline {subsection}{3.1~~~Expression and Methylation Correlation}{10}{section*.16}
\contentsline {subsection}{3.2~~~CGH Data Analysis}{19}{section*.18}
\contentsline {subsubsection}{3.2.1~~~Quality control issues}{19}{section*.20}
\contentsline {subsubsection}{3.2.2~~~Data segmentation}{26}{section*.22}
\contentsline {subsubsection}{3.2.3~~~Centering the CGH data}{28}{section*.24}
\contentsline {subsubsection}{3.2.4~~~Global Copy Number Behavior}{33}{section*.26}
\contentsline {subsection}{3.3~~~Follow up on interesting finding}{36}{section*.28}
\contentsline {section}{4~~~Conclusions}{43}{section*.30}
\contentsline {section}{5~~~sessionInfo}{44}{section*.32}
