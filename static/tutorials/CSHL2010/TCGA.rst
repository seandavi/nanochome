====================================================
The Cancer Genome Atlas, Glioblastoma Data
====================================================
:Author: Sean Davis
:Contact: sdavis2@mail.nih.gov
:date: |date|

.. |date| date::

.. sectnum::

.. contents:: Table of Contents

Background
==========
Approximately 85-90% of all primary central nervous system tumors arise in the brain [#]_.  The annual incidence of all brain tumors is about 6-7 per 100,000 persons per year with a mortality of about 5 per 100,000 persons per year.  Glioblastoma multiforme is the most common brain tumor in humans, accounting for about 15% of all brain tumors, and generally affects adults, though children may also develop these tumors, also.  The peak incidence is around 60 years of age.  The disease is, in general, devastating with a mean survival of less than one year.  Surgery and radiation are the primary therapeutic modalities though chemotherapy may be used for diffuse disease such as leptomeningeal seeding or positive CSF.

.. [#] See http://cancer.gov/ for details

From `the Cancer Genome Atlas website <http://cancergenome.nih.gov/>`_:

    The Cancer Genome Atlas (TCGA) is a comprehensive and coordinated effort to accelerate our understanding of the molecular basis of cancer through the application of genome analysis technologies, including large-scale genome sequencing. TCGA is a joint effort of the National Cancer Institute (NCI) and the National Human Genome Research Institute (NHGRI), two of the 27 Institutes and Centers of the National Institutes of Health, U.S. Department of Health and Human Services.

    TCGA started as a pilot project in 2006 to assess and validate the feasibility of a full-scale effort to systematically explore the entire spectrum of genomic changes involved in human cancers. With the success of the pilot project, TCGA now will expand its efforts to aggressively pursue 20 or more additional cancers to yield a comprehensive, rigorous and publicly accessible data set that will improve our ability to diagnose, treat and prevent cancer.

GBM is one of the tumors profiled in the pilot study.  As such, there is quite a bit of profiling data available.  I have compiled a subset of these data for experimentation.

*Comment*:  Since this is the last lab of the course, `please experiment`, use the help() function liberally to learn more about the objects, classes, and functions being used.  Also, if there are biological tangents to follow, please do so.  

This vignette is designed to use the `TCGAGBM` data package.  It should be installed prior to getting started.



The Data
========
Based on the availability of data for the 27k Illumina methylation platform, data were downloaded from the TCGA website.  The data are available in the TCGAGBM data package in the extdata directory.  The clinical data will are available, also.  Here is a quick overview:

.. code-block:: r

  > library(TCGAGBM)
  > clinical = read.delim(system.file("extdata/TCGA_GBM_Clinical/clinical_patient_public_GBM.txt.gz", 
  +     package = "TCGAGBM"), header = TRUE)
  > rownames(clinical) = clinical[, 1]
  > summary(clinical)
      BCRPATIENTBARCODE TUMORTISSUESITE    GENDER  
   TCGA-02-2466: 1      BRAIN:44        FEMALE:16  
   TCGA-02-2470: 1      null : 1        MALE  :28  
   TCGA-02-2483: 1                      null  : 1  
   TCGA-02-2485: 1                                 
   TCGA-02-2486: 1                                 
   TCGA-06-2557: 1                                 
   (Other)     :39                                 
   PRETREATMENTHISTORY
   NO  :43            
   null: 2            
                      
                      
                      
                      
                      
                          HISTOLOGICALTYPE
   null                           : 3     
   Untreated primary (De Nova) GBM:42     
                                          
                                          
                                          
                                          
                                          
   PRIORGLIOMA   VITALSTATUS  DAYSTOBIRTH
   NO  :44     DECEASED:24   null   : 2  
   null: 1     LIVING  :19   -12060 : 1  
               null    : 2   -12685 : 1  
                             -13368 : 1  
                             -15964 : 1  
                             -16301 : 1  
                             (Other):38  
    DAYSTODEATH DAYSTOLASTFOLLOWUP
   null   :21   316    : 2        
   133    : 1   86     : 2        
   142    : 1   null   : 2        
   149    : 1   -106   : 1        
   15     : 1   13     : 1        
   182    : 1   133    : 1        
   (Other):19   (Other):36        
   INFORMEDCONSENTVERIFIED
   YES:45                 
                          
                          
                          
                          
                          
                          
   AGEATINITIALPATHOLOGICDIAGNOSIS RADIATIONTHERAPY
   53     : 4                      NO  : 3         
   59     : 3                      null: 3         
   65     : 3                      YES :39         
   52     : 2                                      
   63     : 2                                      
   64     : 2                                      
   (Other):29                                      
   CHEMOTHERAPY IMMUNOTHERAPY HORMONALTHERAPY
   NO  : 7      NO  :40       NO  :26        
   null: 5      null: 4       null: 4        
   YES :33      YES : 1       YES :15        
                                             
                                             
                                             
                                             
   TARGETEDMOLECULARTHERAPY DAYSTOTUMORPROGRESSION
   NO  :39                  null   :28            
   null: 5                  119    : 1            
   YES : 1                  128    : 1            
                            135    : 1            
                            143    : 1            
                            153    : 1            
                            (Other):12            
   DAYSTOTUMORRECURRENCE SITEOFTUMORFIRSTRECURRENCE
   78  : 1               null:45                   
   null:44                                         
                                                   
                                                   
                                                   
                                                   
                                                   
   TUMORSTAGE TUMORGRADE RESIDUALTUMOR
   null:45    null:45    null:45      
                                      
                                      
                                      
                                      
                                      
                                      
   TUMORRESIDUALDISEASE
   null:45             
                       
                       
                       
                       
                       
                       
   PRIMARYTHERAPYOUTCOMESUCCESS
   null:45                     
                               
                               
                               
                               
                               
                               
   ADDITIONALRADIATIONTHERAPY
   NO  :39                   
   null: 2                   
   YES : 4                   
                             
                             
                             
                             
   ADDITIONALCHEMOTHERAPY ADDITIONALIMMUNOTHERAPY
   NO  :33                NO  :43                
   null: 2                null: 2                
   YES :10                                       
                                                 
                                                 
                                                 
                                                 
   ADDITIONALHORMONETHERAPY ADDITIONALDRUGTHERAPY
   NO  :43                  NO  :42              
   null: 2                  null: 3              
                                                 
                                                 
                                                 
                                                 
                                                 
   ANATOMICORGANSUBDIVISION
   Brain:39                
   null : 6                
                           
                           
                           
                           
                           
                INITIALPATHOLOGICDIAGNOSISMETHOD
   EXCISIONAL BIOPSY            : 4             
   FINE NEEDLE ASPIRATION BIOPSY: 1             
   null                         : 1             
   TUMOR RESECTION              :39             
                                                
                                                
                                                
   PERSONNEOPLASMCANCERSTATUS    X          
   null      : 7              Mode:logical  
   TUMOR FREE: 1              NA's:45       
   WITH TUMOR:37                             



Methylation data preparation
----------------------------
The methylation data were not in any standard Illumina data dump format, so some custom code is necessary to convert to a MethyLumiSet object.  

.. code-block:: r

  > tmp = sapply(dir(system.file("extdata/TCGA_GBM_IlluminaMethylation27k", 
  +     package = "TCGAGBM"), pattern = "Methylation27.7"), 
  +     function(x) {
  +         message(x)
  +         read.delim(file.path(system.file("extdata/TCGA_GBM_IlluminaMethylation27k", 
  +             package = "TCGAGBM"), x), 
  +             header = TRUE, skip = 1)
  +     }, simplify = FALSE)
  > matrixnames = colnames(tmp[[1]])
  > matrixlist = lapply(matrixnames, function(x) {
  +     message(x)
  +     sapply(tmp, function(y) y[, x])
  + })
  > names(matrixlist) <- matrixnames
  > b = matrixlist$Methylated_Signal_Intensity..M./(matrixlist$Methylated_Signal_Intensity..M. + 
  +     matrixlist$Un.Methylated_Signal_Intensity..U. + 
  +     100)
  > methTCGA = new("MethyLumiSet", betas = b)
  > for (x in names(matrixlist)) {
  +     message(x)
  +     assayDataElement(methTCGA, x) = matrixlist[[x]]
  + }
  > featureNames(methTCGA) <- assayDataElement(methTCGA, 
  +     "Composite.Element.REF")[, 1]
  > sampleNames(methTCGA) = substr(sub("jhu-usc.edu_GBM.HumanMethylation27.7.lvl-1.", 
  +     "", sampleNames(methTCGA)), 1, 12)
  > pData(methTCGA) = clinical[match(clinical[, 
  +     1], sampleNames(methTCGA)), ]
  > annotation(methTCGA) = "IlluminaHumanMethylation27k"
  > experimentData(methTCGA) = new("MIAME", 
  +     name = "TCGA GBM Methylation 27k data", 
  +     lab = "TCGA, JHU methylation", url = "http://cancergenome.nih.gov/")



Now, to look at what we have just loaded:




.. code-block:: r

  > methTCGA
  Object Information:
  MethyLumiSet (storageMode: lockedEnvironment)
  assayData: 27578 features, 45 samples 
    element names: betas, Composite.Element.REF, Detection_P_Value, M_Number_Beads, M_STDERR, Methylated_Signal_Intensity..M., Negative_Control_Grn_Avg_Intensity, Negative_Control_Grn_STDERR, Negative_Control_Red_Avg_Intensity, Negative_Control_Red_STDERR, U_Number_Beads, U_STDERR, Un.Methylated_Signal_Intensity..U. 
  protocolData: none
  phenoData
    sampleNames: TCGA-02-2466, TCGA-02-2470, ..., TC
    GA-32-1986  (45 total)
    varLabels and varMetadata description:
      BCRPATIENTBARCODE: NA
      TUMORTISSUESITE: NA
      ...: ...
      X: NA
      (34 total)
  featureData: none
  experimentData: use 'experimentData(object)'
  Annotation: IlluminaHumanMethylation27k 
  Major Operation History:
  [1] submitted finished  command  
  <0 rows> (or 0-length row.names) 
  > experimentData(methTCGA)
  Experiment data
    Experimenter name: TCGA GBM Methylation 27k data 
    Laboratory: TCGA, JHU methylation 
    Contact information:  
    Title:  
    URL: http://cancergenome.nih.gov/ 
    PMIDs:  
    No abstract available. 



CGH data preparation
--------------------
Data from the same patients as above were downloaded from the TCGA website.  Briefly, the samples were run on 244k Agilent long oligo arrays with Promega commercial DNA run as a reference.  We can use the limma package to load and manipulate the data.



.. code-block:: r

  > library(limma)
  > tmp2 = read.maimages(files = dir(system.file("extdata/TCGA_GBM_244kcgh", 
  +     package = "TCGAGBM"), pattern = "MSK"), 
  +     path = system.file("extdata/TCGA_GBM_244kcgh", 
  +         package = "TCGAGBM"), source = "agilent")



A little convenience function, splitAgilentChromLocs is useful to get the chromosome locations from the feature extraction data.

.. code-block:: r

  > splitAgilentChromLocs <- function(systematicName) {
  +     tmp <- gsub("[:-]", ":", as.character(systematicName))
  +     tmp2 <- data.frame(do.call(rbind, 
  +         strsplit(as.character(tmp), ":")))
  +     colnames(tmp2) <- c("chrom", "start", 
  +         "end")
  +     tmp2[, 2] = as.integer(as.character(tmp2[, 
  +         2]))
  +     tmp2[, 3] = as.integer(as.character(tmp2[, 
  +         3]))
  +     return(tmp2)
  + }
  > locs = splitAgilentChromLocs(as.character(tmp2$genes$SystematicName))
  > tmp2$genes = data.frame(tmp2$genes, locs)



Remove the control probes and "normalize"--that is, convert to MA from RG.  

.. code-block:: r

  > tmp2 = tmp2[tmp2$genes$ControlType == 
  +     0, ]
  > cghTCGAMA = normalizeWithinArrays(tmp2, 
  +     method = "none", bc.method = "none")



Now, map the sample names back to the arrays.

.. code-block:: r

  > cghsdrf = read.delim(system.file("extdata/TCGA_GBM_244kcgh/mskcc.org_GBM.HG-CGH-244A.1.sdrf.txt", 
  +     package = "TCGAGBM"))
  > cghsdrf = cghsdrf[cghsdrf$Provider == 
  +     "BCR", ]
  > cghTCGAMA$targets$sample = substr(cghsdrf[match(rownames(cghTCGAMA$targets), 
  +     cghsdrf$Array.Data.File), 1], 1, 12)
  > cghTCGAMA$targets = data.frame(cghTCGAMA$targets, 
  +     clinical[match(cghTCGAMA$targets$sample, 
  +         clinical[, 1]), ])



And finally, just to make things easier in the future, order the probes in chromosome order and remove probes mapping to odd places like chr7_random, etc.

.. code-block:: r

  > numericchrom = sub("chr", "", cghTCGAMA$genes$chrom)
  > numericchrom[numericchrom == "X"] = 23
  > numericchrom[numericchrom == "Y"] = 24
  > numericchrom = as.integer(numericchrom)
  > cghTCGAMA = cghTCGAMA[!is.na(numericchrom), 
  +     ]
  > numericchrom = numericchrom[!is.na(numericchrom)]
  > cghTCGAMA = cghTCGAMA[order(numericchrom, 
  +     cghTCGAMA$genes$start), ]
  > cghTCGAMA$genes$chrom = factor(as.character(cghTCGAMA$genes$chrom))
  > cghTCGAMA$genes$Chr = numericchrom
  > cghTCGAMA$genes$Position = as.numeric(cghTCAGAMA$genes$start)



Just to get a rough idea of how things look, let's make a plot of one of the samples:

.. code-block:: r

  > plot(cghTCGAMA$M[, 3], pch = ".", col = cghTCGAMA$genes$chrom)


.. image:: TCGA-chromplot.jpg

Expression data preparation
---------------------------
Again, the data were downloaded from the TCGA website.  A rather unusual array platform was used for the gene expression analysis for which there was actually no array description file available.  The array is a custom Agilent 244k array with multiple probes per gene.  However, the TCGA project does make available processed data for these samples.  It appears that the processing was pretty standard with the multiple probes per gene averaged and then loess normalized.  In any case, for our purposes, the data are probably good enough for comparison to other data types.  

.. code-block:: r

  > dat1 = read.delim(system.file("extdata/TCGA_GBM_geneexp/unc.edu__AgilentG4502A_07_2__gene_expression_analysis.txt", 
  +     package = "TCGAGBM"), header = TRUE)
  > datmat = matrix(as.numeric(as.character(dat1$value)), 
  +     nrow = nrow(dat1)/length(unique(dat1$barcode)))
  > colnames(datmat) = substr(unique(dat1$barcode), 
  +     1, 12)
  > rownames(datmat) = dat1$gene.symbol[1:(nrow(dat1)/length(unique(dat1$barcode)))]
  > expTCGA = new("ExpressionSet", exprs = datmat)
  > experimentData(expTCGA) = new("MIAME", 
  +     name = "TCGA GBM level 3 expression data", 
  +     lab = "TCGA, UNC gene expression", 
  +     url = "http://cancergenome.nih.gov/")
  > pData(expTCGA) = clinical[match(sampleNames(expTCGA), 
  +     clinical[, 1]), ]





Analyses
========
Now that the data are loaded and organized, it is time to begin some analysis.  Note that I supply some suggested workflows, but there are plenty of tangents that might be worth following.  

Expression and Methylation Correlation
--------------------------------------
It is interesting, of course, to examine directly the effects of DNA methylation on gene expression.  Armed with our DNA methylation data and our gene expression data, we can directly calculate correlations between the two data types.  In order to perform the calculations, we will want to generate two matrices, one for gene expression and one for DNA methylation.  Each matrix will need to have the same ordering of samples so that we can calculate on them directly.  Also, we will want to map the DNA methylation probes to their matching gene expression probes.  Recall that the gene expression data are in the form on an ExpressionSet and that the featureNames of the ExpressionSet are the actual HGNC gene names.

.. code-block:: r

  > data(expTCGA)
  > featureNames(expTCGA)[10:20]
   [1] "A4GNT"   "AAAS"    "AACS"    "AADAC"  
   [5] "AADACL1" "AADACL2" "AADACL3" "AADACL4"
   [9] "AADAT"   "AAK1"    "AAMP"    



For the methylation data, on the other hand, the annotation for the probes is not directly available from the MethyLumiSet object, methTCGA.  Instead, the featureNames are Illumina probe names.  

.. code-block:: r

  > data(methTCGA)
  > featureNames(methTCGA)[10:20]
   [1] "cg00010193" "cg00011459" "cg00012199"
   [4] "cg00012386" "cg00012792" "cg00013618"
   [7] "cg00014085" "cg00014837" "cg00015770"
  [10] "cg00016968" "cg00019495" 



The appropriate data package is the IlluminaHumanMethylation27k.db package.  It may be necessary to install it using biocLite if it is not already installed.  Attempting to load the package will show if it is installed.  

.. code-block:: r

  > require(IlluminaHumanMethylation27k.db)



As with other annotation packages, we can quickly look to see what is inside:

.. code-block:: r

  > IlluminaHumanMethylation27k()
  Quality control information for IlluminaHumanMethylation27k:
  
  
  This package has the following mappings:
  
  IlluminaHumanMethylation27kACCNUM has 27551 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kALIAS2PROBE has 57212 mapped keys (of 110391 keys)
  IlluminaHumanMethylation27kCHR has 25976 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kCHRLENGTHS has 93 mapped keys (of 93 keys)
  IlluminaHumanMethylation27kCHRLOC has 25948 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kCHRLOCEND has 25948 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kENSEMBL has 25860 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kENSEMBL2PROBE has 14496 mapped keys (of 19971 keys)
  IlluminaHumanMethylation27kENTREZID has 25976 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kENZYME has 3467 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kENZYME2PROBE has 855 mapped keys (of 912 keys)
  IlluminaHumanMethylation27kGENENAME has 25976 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kGO has 24428 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kGO2ALLPROBES has 12023 mapped keys (of 12327 keys)
  IlluminaHumanMethylation27kGO2PROBE has 8841 mapped keys (of 9168 keys)
  IlluminaHumanMethylation27kMAP has 25911 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kOMIM has 19955 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kPATH has 8131 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kPATH2PROBE has 200 mapped keys (of 200 keys)
  IlluminaHumanMethylation27kPFAM has 25957 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kPMID has 25964 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kPMID2PROBE has 250823 mapped keys (of 265573 keys)
  IlluminaHumanMethylation27kPROSITE has 25957 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kREFSEQ has 25976 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kSYMBOL has 25976 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kUNIGENE has 25959 mapped keys (of 27578 keys)
  IlluminaHumanMethylation27kUNIPROT has 25906 mapped keys (of 27578 keys)
  
  
  Additional Information about this package:
  
  DB schema: HUMANCHIP_DB
  DB schema version: 2.1
  Organism: Homo sapiens
  Date for NCBI data: 2010-Mar1
  Date for GO data: 20100320
  Date for KEGG data: 2010-Feb28
  Date for Golden Path data: 2009-Jul5
  Date for IPI data: 2010-Feb10
  Date for Ensembl data: 2010-Mar3 



Since we want to map methylation and gene expression to each other, we can use the annotation package to get gene names for the methylation data.

.. code-block:: r

  > methgenenames = unlist(mget(featureNames(methTCGA), 
  +     IlluminaHumanMethylation27kSYMBOL, 
  +     ifnotfound = NA))
  > sum(is.na(methgenenames))
  [1] 1611 



There are 1611 probes on the methylation platform that, for whatever reason, do not have associated gene symbols.  We will simply exclude those from downstream analysis.  The mapping between platforms is now pretty straightforward.

.. code-block:: r

  > tmp = match(methgenenames, featureNames(expTCGA))
  > methdat = betas(methTCGA)[!is.na(tmp), 
  +     order(sampleNames(methTCGA))]
  > expdat = exprs(expTCGA)[tmp[!is.na(tmp)], 
  +     order(sampleNames(expTCGA))]



Here, we are relying on the fact that the sample names are the same between the two data sets so that ordering by sampleNames will result in matching orders.  We can quickly double-check that that is the case.

.. code-block:: r

  > match(colnames(expdat), colnames(methdat))
   [1]  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
  [16] 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
  [31] 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 
  > dim(expdat)
  [1] 23904    45 
  > dim(methdat)
  [1] 23904    45 



The sample names appear to match and the matrices are the same size.  Now, we are ready to calculate some correlations.  The statement below simply does a row wise Pearson correlation calculation.  The result will be one correlation value per row of data, each of which corresponds to a methylation probe and its associated expression probe.  Note that each expression probe may map to several methylation probes.

.. code-block:: r

  > x = sapply(1:nrow(methdat), function(i) cor(methdat[i, 
  +     ], expdat[i, ]))



So, what would we expect this distribution to look like?  

.. code-block:: r

  > summary(x)
      Min.  1st Qu.   Median     Mean  3rd Qu. 
  -0.92580 -0.20590 -0.05306 -0.05881  0.09256 
      Max.     NA's 
   0.83900 10.00000  



The mean is less than 0 and there is a definite skew to the left.  How big is the effect?  In order to determine what the distribution might look like under the null hypothesis, we can simply permute one of the sample sets relative to the other, effectively breaking any correlation that might exist.

.. code-block:: r

  > y = sapply(1:nrow(methdat), function(i) cor(methdat[i, 
  +     ], expdat[i, sample(1:45, 45)]))
  > summary(y)
        Min.    1st Qu.     Median       Mean 
  -0.6681000 -0.1037000 -0.0019640 -0.0005753 
     3rd Qu.       Max.       NA's 
   0.1030000  0.5739000 10.0000000  



Though this is only one replicate of randomization, it is obvious that this distribution looks more like what we would expect if there were no effect of DNA methylation on gene expression.  A quick plot is probably useful to examine the differences more globally.

.. code-block:: r

  > plot(density(y[!is.na(y)]), col = "green", 
  +     main = "Pearson Correlation")
  > lines(density(x[!is.na(x)]), col = "red")
  > legend(0.3, 2, legend = c("expected", 
  +     "observed"), col = c("green", "red"), 
  +     lty = 1)


.. image:: TCGA-pearsonplot.jpg

Note the shoulder to the left in the 'observed' data, indicating an effect of DNA methylation on gene expression.  One could also do some hypothesis testing to determine if the effect is statistically significant.

There appears to be a global effect of DNA methylation on gene expression.  Which genes show the largest effect?  

.. code-block:: r

  > mincor = rownames(methdat)[which.min(x)]
  > mget(mincor, IlluminaHumanMethylation27kSYMBOL)
  $cg15933546
  [1] "DNAH8" 



And what does a plot of gene expression and DNA methylation look like for that probe?

.. code-block:: r

  > plot(methdat[which.min(x), ], expdat[which.min(x), 
  +     ])


.. image:: TCGA-pearsonbest.jpg

So, there appears to be a bit of a problem with our correlation measure.  The Pearson correlation measure will find such outliers quite nicely.  Instead, perhaps we should use a rank-based correlation metric.

.. code-block:: r

  > x = sapply(1:nrow(methdat), function(i) cor(methdat[i, 
  +     ], expdat[i, ], method = "spearman"))
  > summary(x)
      Min.  1st Qu.   Median     Mean  3rd Qu. 
  -0.82600 -0.20010 -0.04559 -0.05110  0.09796 
      Max.     NA's 
   0.72650 10.00000  
  > y = sapply(1:nrow(methdat), function(i) cor(methdat[i, 
  +     ], expdat[i, sample(1:45, 45)], method = "spearman"))
  > summary(y)
        Min.    1st Qu.     Median       Mean 
  -0.5791000 -0.1024000  0.0010210  0.0001465 
     3rd Qu.       Max.       NA's 
   0.1028000  0.6362000 10.0000000  



The same general trend seems to be present using the Spearman correlation coefficient as with the Pearson correlation coefficient.  

.. code-block:: r

  > plot(density(y[!is.na(y)]), col = "green", 
  +     main = "Spearman Correlation")
  > lines(density(x[!is.na(x)]), col = "red")
  > legend(0.3, 2, legend = c("expected", 
  +     "observed"), col = c("green", "red"), 
  +     lty = 1)


.. image:: TCGA-029.jpg

And the plot looks hearly identical.  Did we do any better with finding biologically meaningful top methylation candidates?

.. code-block:: r

  > plot(methdat[which.min(x), ], expdat[which.min(x), 
  +     ])


.. image:: TCGA-spearmanbest.jpg

That looks better.  The shape of the plot is interesting.  Except for five data points, there appears to be a threshold effect whereby genes are more highly expressed only when methylation is very low.  

- Try writing a short function that will take as input an integer that represents the rank in correlation (lowest being the first), the correlation vector, and the two data matrices and produce a plot of the data for that probe.

- Extend the function to include the gene name in the plot.

- Instead of returning the correlation coefficient like above, change the code so that a p.value is returned.  `Hint`: try experimenting with the cor.test function.

Questions for thought:

- How would you go about determining if there is a regional bias in gene expression and DNA methylation?



CGH Data Analysis
-----------------
CGH data are unlike many other types of high-throughput data such as gene expressin or DNA methylation.  The fact that copy number in the genome is `piecewise continuous` is used by copy number segmentation methods to take measurements that are noisy at the individual probe level and `smooth` them in a non-continuous way; that is, these methods try to respect natural changepoints in the data.  We will be using the snapCGH package to facilitate some of these analyses.  Make sure that it is installed.

.. code-block:: r

  > require(snapCGH, quiet = TRUE)



If this doesn't work, go ahead and install using biocLite().

Quality control issues
~~~~~~~~~~~~~~~~~~~~~~

I had already preprocessed the data above into a useful form.  The data set is in the form of a limma MAList.  The snapCGH package takes that as an input.  

.. code-block:: r

  > library(limma)
  > require(snapCGH)
  > data(cghTCGAMA)
  > class(cghTCGAMA)
  [1] "MAList"
  attr(,"package")
  [1] "limma" 



As you can see, the data just loaded are in the form of a limma MAList.  The M values in this MAList are `not` gene expression measures, but log2 ratios of tumor DNA to a reference normal DNA.  For normal individuals, the ratio of sample to reference signal will be close to 1 for a log2 ratio of 0.  Gain of a copy of DNA at a locus will result in a value of log2(3/2) while loss of a copy will show log2(1/2).  The sex chromosomes will behave slightly differently; that behavior will depend on the genders of both the reference and the sample.  

The annotation for these data are in the $genes list element.

.. code-block:: r

  > head(cghTCGAMA$genes)
         Row Col ProbeUID ControlType
  8437    19 233     8589           0
  25100   56  31    24444           0
  61506  135 419    61188           0
  48639  107 318    48398           0
  69696  153 403    69245           0
  107488 236 354   105927           0
              ProbeName                 GeneName
  8437     A_14_P112718                 AK026901
  25100  A_16_P15000916                 AK026901
  61506  A_16_P15001074                 AK125248
  48639  A_16_P00000012 chr1:000736483-000736542
  69696  A_16_P00000014 chr1:000742533-000742586
  107488 A_16_P00000017 chr1:000746956-000747005
                   SystematicName
  8437   chr1:000554268-000554327
  25100  chr1:000554287-000554346
  61506  chr1:000639581-000639640
  48639  chr1:000736483-000736542
  69696  chr1:000742533-000742586
  107488 chr1:000746956-000747005
                                                 Description
  8437      Homo sapiens cDNA: FLJ23248 fis, clone COL03555.
  25100     Homo sapiens cDNA: FLJ23248 fis, clone COL03555.
  61506  Homo sapiens cDNA FLJ43258 fis, clone HHDPC1000001.
  48639                                              Unknown
  69696                                              Unknown
  107488                                             Unknown
         chrom  start    end Chr Position
  8437    chr1 554268 554327   1   554268
  25100   chr1 554287 554346   1   554287
  61506   chr1 639581 639640   1   639581
  48639   chr1 736483 736542   1   736483
  69696   chr1 742533 742586   1   742533
  107488  chr1 746956 747005   1   746956 



The data should be sorted by chromosome and position already, so we can plot a whole genome of log2 ratios easily with a single command.  I add color to distinguish the chromosomes.

.. code-block:: r

  > plot(cghTCGAMA$M[, 3], col = cghTCGAMA$genes$Chr, 
  +     pch = ".")


.. image:: TCGA-034.jpg

You can see that most of the values are, indeed, near 0 with some spread.  This spread represents the "noise" in the data and should probably be quantified.  Normally, the spread of such values can be estimated by a measure such as the standard deviation.  This will work just fine if the entire genome has a common mean (no copy number variation).  How do the standard deviations look across our samples? 

.. code-block:: r

  > log2sds = apply(cghTCGAMA$M, 2, sd)
  > barplot(log2sds, main = "Standard Deviations of log2 Ratios", 
  +     xlab = "sample")


.. image:: TCGA-035.jpg

When advising experimentalists of the quality of their CGH arrays, it is important to be able to provide a robust estimate of the noise in the data.  To see if the standard deviation is a very robust estimator of the noise.  Let's consider a hypothetical chromosome with 10000 probes on it and no copy number variation.  We can simulate such a thing quickly in R using rnorm and let's use a standard deviation of 0.4, similar to that in our data.

.. code-block:: r

  > fakechrom = rnorm(10000, sd = 0.4)
  > sd(fakechrom)
  [1] 0.4007763 



Not very interesting, but let's put a copy number change in the middle of the chromosome that is similar in scale to that shown in the sample you plotted above:

.. code-block:: r

  > fakechrom[4900:5000] = fakechrom[4900:5000] + 
  +     4
  > plot(fakechrom, pch = ".")
  > sd(fakechrom)
  [1] 0.5684836 


.. image:: TCGA-037.jpg

Note that the standard deviation is now quite a bit larger than the 0.4 that we had anticipated.  In other words, the standard deviation is sensitive to outliers in the data and with cancer samples, there can be quite a few outliers.  With genomic data of any kind that might be expected to behave in a piecewise constant fashion, one can use another measure of noise, the derivative log ratio spread (DLRS).  Defined here:

.. code-block:: r

  > dlrs <- function(x) {
  +     nx <- length(x)
  +     if (nx < 3) {
  +         stop("Vector length>2 needed for computation")
  +     }
  +     tmp <- embed(x, 2)
  +     diffs <- tmp[, 2] - tmp[, 1]
  +     dlrs <- IQR(diffs, na.rm = TRUE)/(sqrt(2) * 
  +         1.34)
  +     return(dlrs)
  + }



What is the dlrs of our fake chromosome, even with the noise added?

.. code-block:: r

  > dlrs(fakechrom)
  [1] 0.4012613 



And of our samples?

.. code-block:: r

  > log2dlrs = apply(cghTCGAMA$M, 2, dlrs)
  > barplot(log2dlrs, main = "DLRS", xlab = "sample")


.. image:: TCGA-040.jpg

The DLRS is a useful measure of the actual noise in the data and is largely unaffected by "signal".  As such, it is a useful reporting tool for the technical quality of an array.  All of the arrays in our sample set have DLRS values < 0.3, a maximum value recommended by Agilent.

We have applied the dlrs here to two-color Agilent CGH data, but such a measure could also be applied to log2 ratios from SNP arrays or even to copy number estimates generated by second-generation sequencing.

Data segmentation
~~~~~~~~~~~~~~~~~
A key component of making the best use of CGH data is called "segmentation''.  There is extensive literature and literally dozens of methods for performing segmentation on CGH data.  The snapCGH package provides convenient wrappers for many of these methods as implemented in R packages.  One that is particularly popular, easy to use, and fairly robust with not much need for parameter tuning on this data set is Circular Binary Segmentation (CBS) and is implemented in the DNAcopy package.  The snapCGH package provides a wrapper called, conveniently enough, "runDNAcopy".  

.. code-block:: r

  > require(snapCGH)
  > require(DNAcopy)
  > citation("DNAcopy")
  To cite package 'DNAcopy' in publications
  use:
  
    Venkatraman E. Seshan and Adam Olshen ().
    DNAcopy: DNA copy number data analysis. R
    package version 1.23.3.
  
  A BibTeX entry for LaTeX users is
  
    @Manual{,
      title = {DNAcopy: DNA copy number data analysis},
      author = {Venkatraman E. Seshan and Adam Olshen},
      year = {},
      note = {R package version 1.23.3},
    }
  
  ATTENTION: This citation information has
  been auto-generated from the package
  DESCRIPTION file and may need manual
  editing, see 'help("citation")' . 



The citation function can be used with any R package to get a suggested reference or references.  In some cases, the authors will list publications here, also.

The snapCGH and DNAcopy packages are loaded, so we can proceed to the segmentation process.  This process can be rather time-consuming, so I suggest using only a subset of about 3 samples.

.. code-block:: r

  > cghTCGAMA$design = rep(1, ncol(cghTCGAMA))
  > cghTCGAMA123 = cghTCGAMA[, 1:3]



.. code-block:: r

  > dnacopyresult = runDNAcopy(cghTCGAMA123)
  Analyzing: Sample.1 
  Analyzing: Sample.2 
  Analyzing: Sample.3  
  > dim(dnacopyresult)
  [1] 237834      3 
  > class(dnacopyresult)
  [1] "SegList"
  attr(,"package")
  [1] "snapCGH" 



Warning messages are OK here.  Error messages are not.  

The interesting part of the dnacopyresult object is the M.predicted list element.  Perhaps a plot is the most interesting way to look at the data.  First, plot the raw data which is, for convenience, included in the dnacopyresult object in the M.observed list element.  We add to this plot the estimated log2 ratio estimates from the DNAcopy segmentation.

.. code-block:: r

  > plot(dnacopyresult$M.observed[, 1], col = dnacopyresult$genes$Chr, 
  +     pch = ".")
  > lines(dnacopyresult$M.predicted[, 1], 
  +     col = "red")


.. image:: TCGA-044.jpg

At this point, you have successfully segmented (smoothed) your data.  The snapCGH package has other options for segmentation besides runDNAcopy.  Feel free to experiment by using the help function to get other options and learn how to use them.

Centering the CGH data
~~~~~~~~~~~~~~~~~~~~~~
Remember that our goal is to find regions of gain and loss.  To do so, we want to make sure that the "baseline" for all the samples is near a log2 ratio of zero.  I include "baseline" in quotes because the baseline represents our estimate of the "normal" copy number state.  An assumption that I and others will make is that the best estimate of the "normal" state is that which minimizes the number of probes that are not near the log2 ratio of 0.  

Perhaps a visual representation is useful here.  For this part of the exercise, we will be using DNAcopy segmented values that I prepared earlier.  So, load the data:

.. code-block:: r

  > data(DNAcopySegList)
  > dim(DNAcopySegList)
  [1] 237834     45 
  > class(DNAcopySegList)
  [1] "SegList"
  attr(,"package")
  [1] "snapCGH" 



And plot one example where the center is probably not correct:

.. code-block:: r

  > plot(DNAcopySegList$M.observed[, 1], pch = ".", 
  +     col = DNAcopySegList$genes$Chr)


.. image:: TCGA-046.jpg

Notice that the "center" of the distribution is at log2 ratio of -0.6.  How can we reliably and robustly find the "center" of this distribution?  Let's look at a density plot of the `segmented` data.

.. code-block:: r

  > plot(density(DNAcopySegList$M.predicted[, 
  +     1]), xlim = c(-1.5, 1.5))


.. image:: TCGA-047.jpg

The mode of this distribution is exactly what we are interested in using as the "center".  Therefore, I include a little function to find the mode of a vector of numbers.

.. code-block:: r

  > findMode = function(z) {
  +     tmpdens = density(z)
  +     return(tmpdens$x[which.max(tmpdens$y)])
  + }



We can apply it to all samples simultaneously using the apply functionality of R.

.. code-block:: r

  > ctrs = apply(DNAcopySegList$M.predicted, 
  +     2, findMode)
  > summary(ctrs)
      Min.  1st Qu.   Median     Mean  3rd Qu. 
  -0.71250 -0.28210 -0.12500 -0.16040 -0.02486 
      Max. 
   0.16420  



To "fix" the data (that is, subtract the offsets from the data to appropriately "center" them), we can use the sweep function to add the appropriate offset to the data to get the "baseline" near the log2 of 0, our goal.

.. code-block:: r

  > DNAcopySegList$M.predicted = sweep(DNAcopySegList$M.predicted, 
  +     2, ctrs)
  > DNAcopySegList$M.observed = sweep(DNAcopySegList$M.observed, 
  +     2, ctrs)



Now, if we plot the same sample as before, it should show a baseline close to log2 of 0.

.. code-block:: r

  > plot(DNAcopySegList$M.observed[, 1], pch = ".", 
  +     col = DNAcopySegList$genes$Chr)


.. image:: TCGA-051.jpg

Now, on to biology, finally!

Global Copy Number Behavior
~~~~~~~~~~~~~~~~~~~~~~~~~~~
Now that we have performed segmentation, thereby smoothing the data while maintaining the natural breakpoints in the data, we want to look for the regions with the highest proportion of copy number changes in the samples.  Particularly if we successfully remove common copy number variants that are present in normal individuals, such regions may be thought to be the most interesting biologically.  

There are a number of methods described in the literature to find such "interesting'' regions.  However, we will keep it simple here and try just a couple.  Using our segmented and centered data, let's simply add up the log2 ratios at each locus for every sample.  

.. code-block:: r

  > log2sums = rowSums(DNAcopySegList$M.predicted)
  > length(log2sums)
  [1] 237834 



And plot the result.

.. code-block:: r

  > plot(log2sums, pch = ".", col = DNAcopySegList$genes$Chr)


.. image:: TCGA-053.jpg

This figure is very important to understand.  The distance away from the y=0 line, here simply the sum of the segmented log2 ratios, is somewhat a measure of biological importance.  Recall that regions that are gained may contain oncogenes while regions with loss are potentially harboring tumor suppressors.  A few regions of interest immediately pop out.  There are small, high excursions on chromosomes 7 and 12.  There is a significant low excursion on chromosome 9 (and also chromosome Y, which we will choose to ignore, but why would chromosome Y appear to be deleted?).  Also, though lower excursion, the baseline for chromosome 7 shows a gain while the baseline for chromosome 10 shows a loss.  Does this make sense biologically given what is known about glioblastoma?

This is all well-and-good, but what genes are in the regions of high gain and loss?  Well, let's just ask the data.  Let's find the probes that correspond to the regions of highest gain and lowest loss.  To do so, I will use a simple threshold and pull out the probes that have log2sums higher than the threshold.  In these data, the thresholds are quite easy to choose; this is not, in general, the case.

.. code-block:: r

  > highcopygenes = DNAcopySegList$genes[log2sums > 
  +     20, c("Chr", "GeneName")]
  > unique(highcopygenes[-grep("chr", highcopygenes$GeneName), 
  +     ])
         Chr GeneName
  210431   7 FLJ45974
  65369    7    VSTM2
  174544   7 CR613464
  140801   7 BC045679
  241146   7   SEC61G
  165548   7     EGFR
  44942    7   K03193
  91766    7   LANCL2
  93272    7 AK128355
  121710   7     ECOP
  125336   7 BC015339
  192911   7 FLJ44060
  209927   7 BC094796
  130609   7   ZNF713
  135653   7 CR590495
  155510   7   MRPS17
  156592   7     GBAS
  29164    7     PSPH
  210171   7    CCT6A
  225314   7    SUMF2
  119849   7    PHKG1
  1910     7   Y10275
  190999   7   CHCHD2
  128900   7 AL713776
  37842    7 BC035176
  193160  12      OS9
  43012   12   CENTG1
  205540  12  TSPAN31
  72692   12     CDK4
  3017    12   MARCH9
  67423   12 AK093897
  85074   12  CYP27B1
  195380  12   METTL1
  155592  12  FAM119B
  158880  12     TSFM
  135524  12     AVIL 



For those not in the cancer field, I point out two important cancer genes.  The high copy number region on chromosome 7 is harboring an EGFR amplification.  Just check the literature for EGFR amplification in glioblastoma!  On chromosome 12, CDK4 is in the amplified region and is an important cell cycle gene.  

What about the region of loss?

.. code-block:: r

  > lowcopygenes = DNAcopySegList$genes[log2sums < 
  +     (-30), c("Chr", "GeneName")]
  > unique(lowcopygenes[-grep("chr", lowcopygenes$GeneName), 
  +     ])
         Chr GeneName
  81796    9     MTAP
  107171   9 AF109294
  166419   9   CDKN2A
  41673    9   CDKN2B
  10694   24   RBMY1F 



Again, for non-cancer folks, CDKN2A is a well-known tumor suppressor!  

Follow up on interesting finding
--------------------------------

So, we have three important cancer genes popping up by basic visual inspection!  But, one might begin to wonder a bit about the relationship between these genes.  In fact, since there were only three genes that were viable candidates and EGFR was a well-known finding in glioblastoma, I decided to dig a bit further with CDK4 and CDKN2A.  

I'll leave it as an exercise to the reader to learn extensively about these two genes and their interactions, but suffice it to say that the full gene name of CDKN2A is "cyclin-dependent kinase inhibitor 2A (melanoma, p16, inhibits CDK4)".  CDK4 drives the cell cycle, roughly, and CDKN2A serves to negatively regulate that effect.  CDK4 is the prototypic oncogene and CDKN2A is the prototypic tumor suppressor, suppressing tumors by negatively regulating CDK4.

Back to the data, we want to look at the relationship between copy number estimates of CDKN2A and CDK4.  I will choose representative probes for each gene and look at a plot of them together.

.. code-block:: r

  > cdk4probeidx = which(DNAcopySegList$genes$GeneName == 
  +     "CDK4")[1]
  > cdkn2aprobeidx = which(DNAcopySegList$genes$GeneName == 
  +     "CDKN2A")[1]
  > plot(DNAcopySegList$M.predicted[cdk4probeidx, 
  +     ], DNAcopySegList$M.predicted[cdkn2aprobeidx, 
  +     ], xlab = "CDK4 log2 ratio", ylab = "CDKN2A log2 ratio")


.. image:: TCGA-056.jpg

How would you interpret the plot?  Does this make sense given what you know about CDKN2A and CDK4 biology?

Perhaps we want to look at the effect of copy number on gene expression for these genes.  To do so, we need the copy number data to be in the same order as the gene expression data.  

.. code-block:: r

  > cnCDK4 = DNAcopySegList$M.predicted[cdk4probeidx, 
  +     order(cghTCGAMA$targets$BCRPATIENTBARCODE)]
  > cnCDKN2A = DNAcopySegList$M.predicted[cdkn2aprobeidx, 
  +     order(cghTCGAMA$targets$BCRPATIENTBARCODE)]
  > exCDK4 = exprs(expTCGA)["CDK4", order(sampleNames(expTCGA))]
  > exCDKN2A = exprs(expTCGA)["CDKN2A", order(sampleNames(expTCGA))]



And now, we can make a plot of the two genes to see how things look:

.. code-block:: r

  > par(mfrow = c(2, 1))
  > plot(cnCDK4, exCDK4, main = "CDK4", xlab = "Copy Number", 
  +     ylab = "Gene Expression")
  > plot(cnCDKN2A, exCDKN2A, main = "CDKN2A", 
  +     xlab = "Copy Number", ylab = "Gene Expression")


.. image:: TCGA-058.jpg

And how about methylation, particularly of CDKN2A, since that could be a secondary mechanism for silencing the gene, besides gene deletion?  We'll need to get at least one methylation data vector or CDKN2A.  

.. code-block:: r

  > methGeneNames = unlist(mget(featureNames(methTCGA), 
  +     IlluminaHumanMethylation27kSYMBOL, 
  +     ifnotfound = NA))
  > cdkn2amethidx = which(methGeneNames == 
  +     "CDKN2A")
  > cor(t(betas(methTCGA)[cdkn2amethidx, ]))
             cg00718440 cg03079681 cg13479669
  cg00718440  1.0000000 -0.1392268 -0.1244518
  cg03079681 -0.1392268  1.0000000  0.7301408
  cg13479669 -0.1244518  0.7301408  1.0000000
  cg26673943 -0.1494681  0.8510897  0.7594127
             cg26673943
  cg00718440 -0.1494681
  cg03079681  0.8510897
  cg13479669  0.7594127
  cg26673943  1.0000000 



Note that three out of four probes are correlated positively with each other.  So, we can exclude the first probe because it does not agree with the other three.  This might be an incorrect assumption, but we can always check later.  Using the last probe, arbitrarily, as representative, we can now interrogate the effect of methylation on gene expression of CDKN2A.

.. code-block:: r

  > methCDKN2A = betas(methTCGA)["cg26673943", 
  +     order(sampleNames(methTCGA))]



Finally, let's plot against gene expression and copy number:

.. code-block:: r

  > par(mfrow = c(2, 1))
  > plot(methCDKN2A, exCDKN2A, main = "Expression vs. Methylation", 
  +     sub = "CDKN2A, last probe")
  > plot(methCDKN2A, cnCDKN2A, main = "Copy Number vs. Methylation", 
  +     sub = "CDKN2A, last probe")


.. image:: TCGA-061.jpg

Taking the expression plot, first, we see a very nice negative correlation between expression and methylation, despite the fact that the maximal observed methylation is 0.3 or so.  Looking at the bottom plot, however, it appears that the same correlation holds with copy number.  We might hope to see that the normal copy number samples would be more likely to have higher methylation values.  Instead, the opposite is true.  Remember that we chose to ignore the first methylation probe because it was poorly correlated with the other three methylation probes.  Let's revisit that probe.

.. code-block:: r

  > methCDKN2A = betas(methTCGA)["cg00718440", 
  +     order(sampleNames(methTCGA))]
  > par(mfrow = c(2, 1))
  > plot(methCDKN2A, exCDKN2A, main = "Expression vs. Methylation", 
  +     sub = "CDKN2A, First probe")
  > plot(methCDKN2A, cnCDKN2A, main = "Copy Number vs. Methylation", 
  +     sub = "CDKN2A, First probe")


.. image:: TCGA-062.jpg

While the number of samples with high methylation is small (probably just 2), these samples show lowish CDKN2A gene expression levels.  With regard to copy number, the samples with high methylation are, indeed, those without deletion, suggesting that methylation could be responsible for CDKN2A silencing in these samples.  And what about the CDK4 amplification status of these samples?

.. code-block:: r

  > plot(cnCDK4, methCDKN2A)


.. image:: TCGA-063.jpg

These two samples have stone-cold normal CDK4 copy number.  

Conclusions
===========
We have performed a subset of a data integration exercise on TCGA glioblastoma data and have an incredibly interesting finding within the space of an afternoon.  There is a small but measurable global effect of methylation on gene expression.  Also, we find in the copy number data the most common amplification, EGFR, in these data as well as gain of chromosome 7 and loss of chromosome 10, also common findings in glioblastoma.

At a more mechanistic level, it seems that the relationship between CDKN2A, acting as tumor suppressor and it's target, CDK4 is quite clear in these data.  We have a working hypothesis that CDK4 overexpression due to amplification and CDKN2A underexpression due to either DNA loss (deletion) or methylation at one CpG site are approximately mutually exclusive and likely represent the same pathway being disregulated in these data.  

sessionInfo
===========
.. code-block:: r

  > sessionInfo()
  R version 2.12.0 Under development (unstable) (2010-04-30 r51866) 
  i386-apple-darwin9.8.0 
  
  locale:
  [1] en_US/en_US/C/C/en_US/en_US
  
  attached base packages:
  [1] stats     graphics  grDevices datasets 
  [5] utils     methods   base     
  
  other attached packages:
   [1] snapCGH_1.19.0                      
   [2] DNAcopy_1.23.3                      
   [3] IlluminaHumanMethylation27k.db_1.2.0
   [4] org.Hs.eg.db_2.4.1                  
   [5] RSQLite_0.9-1                       
   [6] DBI_0.2-5                           
   [7] AnnotationDbi_1.11.0                
   [8] TCGAGBM_1.0                         
   [9] limma_3.5.10                        
  [10] methylumi_1.3.3                     
  [11] Biobase_2.9.0                       
  [12] ascii_0.6-4                         
  [13] proto_0.3-8                         
  
  loaded via a namespace (and not attached):
   [1] aCGH_1.27.0           affy_1.27.0          
   [3] affyio_1.17.0         annotate_1.27.0      
   [5] cluster_1.12.3        genefilter_1.31.0    
   [7] GLAD_2.11.0           grid_2.12.0          
   [9] lattice_0.18-8        MASS_7.3-6           
  [11] multtest_2.5.0        preprocessCore_1.11.0
  [13] RColorBrewer_1.0-2    splines_2.12.0       
  [15] strucchange_1.4-0     survival_2.35-8      
  [17] tilingArray_1.27.1    tools_2.12.0         
  [19] vsn_3.17.1            xtable_1.5-6          


