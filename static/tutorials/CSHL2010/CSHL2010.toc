\select@language {english}
\contentsline {chapter}{\numberline {1}Connecting to Public Data Resources}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}NCBI GEO}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Overview of GEO}{3}{subsection.1.2.1}
\contentsline {subsubsection}{Platforms}{4}{subsubsection*.2}
\contentsline {subsubsection}{Samples}{4}{subsubsection*.3}
\contentsline {subsubsection}{Series}{4}{subsubsection*.4}
\contentsline {subsubsection}{Datasets}{4}{subsubsection*.5}
\contentsline {subsection}{\numberline {1.2.2}Getting Started using GEOquery package}{4}{subsection.1.2.2}
\contentsline {subsubsection}{GEOquery Data Structures}{5}{subsubsection*.6}
\contentsline {subsubsection}{Converting to BioConductor ExpressionSets and limma MALists}{8}{subsubsection*.7}
\contentsline {paragraph}{Converting GDS to an ExpressionSet}{8}{paragraph*.8}
\contentsline {paragraph}{Converting GDS to an MAList}{9}{paragraph*.9}
\contentsline {subsubsection}{Getting GSE Series Matrix files as an ExpressionSet}{13}{subsubsection*.10}
\contentsline {subsubsection}{Accessing Raw Data from GEO}{14}{subsubsection*.11}
\contentsline {subsubsection}{Summary of GEOquery}{14}{subsubsection*.12}
\contentsline {subsection}{\numberline {1.2.3}Overview of the GEOmetadb package}{14}{subsection.1.2.3}
\contentsline {subsubsection}{Conversion capabilities}{15}{subsubsection*.13}
\contentsline {subsubsection}{Using GEOmetadb}{16}{subsubsection*.14}
\contentsline {paragraph}{A word about SQL}{16}{paragraph*.15}
\contentsline {paragraph}{GEOmetadb Examples}{17}{paragraph*.16}
\contentsline {chapter}{\numberline {2}Illumina Methylation Data Analysis}{23}{chapter.2}
\contentsline {section}{\numberline {2.1}Introductory Remarks}{23}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}The Illumina Methylation Platform}{23}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Illumina Methylation Data Analysis}{23}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Overview of the Golden Gate Methylation Array}{24}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}A Simple Example}{25}{subsection.2.2.2}
\contentsline {subsubsection}{Loading data}{25}{subsubsection*.17}
\contentsline {subsubsection}{Quality Control}{28}{subsubsection*.18}
\contentsline {subsubsection}{Normalization}{30}{subsubsection*.19}
\contentsline {subsubsection}{Example Analysis}{31}{subsubsection*.20}
\contentsline {subsection}{\numberline {2.2.3}Methylation Profiles of Breast Cancer}{33}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}sessionInfo}{42}{section.2.3}
\contentsline {chapter}{\numberline {3}The Cancer Genome Atlas, Glioblastoma Data}{43}{chapter.3}
\contentsline {section}{\numberline {3.1}Background}{43}{section.3.1}
\contentsline {section}{\numberline {3.2}The Data}{44}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Methylation data preparation}{46}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}CGH data preparation}{48}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Expression data preparation}{50}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Analyses}{50}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Expression and Methylation Correlation}{50}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}CGH Data Analysis}{57}{subsection.3.3.2}
\contentsline {subsubsection}{Quality control issues}{58}{subsubsection*.21}
\contentsline {subsubsection}{Data segmentation}{63}{subsubsection*.22}
\contentsline {subsubsection}{Centering the CGH data}{65}{subsubsection*.23}
\contentsline {subsubsection}{Global Copy Number Behavior}{69}{subsubsection*.24}
\contentsline {subsection}{\numberline {3.3.3}Follow up on interesting finding}{72}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Conclusions}{78}{section.3.4}
\contentsline {section}{\numberline {3.5}sessionInfo}{79}{section.3.5}
