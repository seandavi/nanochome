# All files in the 'lib' directory will be loaded
# before nanoc starts compiling.

# lib/default.rb

require 'org-ruby'
require 'fileutils'
require 'rbst'

#include Nanoc3::Helpers::Blogging
include Nanoc3::Helpers::Breadcrumbs
include Nanoc3::Helpers::Capturing
include Nanoc3::Helpers::Filtering
include Nanoc3::Helpers::HTMLEscape
include Nanoc3::Helpers::LinkTo
include Nanoc3::Helpers::Rendering
include Nanoc3::Helpers::Tagging
include Nanoc3::Helpers::Text
include Nanoc3::Helpers::XMLSitemap
#include Nanoc3::Filter
include FileUtils

# A simple restructuredText filter
class RstFilter < Nanoc3::Filter
  identifier :rst
  type :text

  def run(content,params={})
    RbST.new(content).to_html
  end
end

# An org-mode filter based on using aquamacs!
class OrgFilter < Nanoc3::Filter
  identifier :org
  type :binary
  
  def run(filename, params={})
    system('/Applications/Aquamacs.app/Contents/MacOS/Aquamacs',
           '-nw',
           '--batch',
           '--eval',
           "(setq aquamacs-version 2)",
           '--visit',
           filename,
           '--funcall',
           'org-export-as-html')
    FileUtils.mv(filename.gsub('.org','.html'),output_filename)
  end
end

# This is just some syntactical sugar that we use later
# Don't worry about it for now.
class Nanoc3::Item
  def content(opts = {})
    opts[:rep] ||= :default
    opts[:snapshot] ||= :last
    reps.find { |r| r.name == opts[:rep] }.content_at_snapshot(opts[:snapshot])
  end

  def name
    identifier.split("/").last 
  end
end
